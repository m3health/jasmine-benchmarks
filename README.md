# Jasmine Performance Benchmarks
## from https://trello.com/c/gnkZkoZ2/229-create-performance-requirements-for-import-export-apis

Run a suite of tests that provide some information about the speed of the Jasmine web services.

First, install all required dependencies with:

```
npm install
```

In order to run this project, you must have node version 11 or up and run it with the `--harmony` flag:

```
node --harmony index.js
```