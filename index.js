//
// Dependencies
//
var
  fs = require('fs')
  , config = require('./config.js')
  , bm = require('benchmark')
  , req = require('request')
  // , async = require('async')
  , suite = new bm.Suite;


//
// Configure Benchmark.js options http://benchmarkjs.com/docs#options
//
// bm.options.name = 'Jasmine Services Performance Test';
// bm.options.minSamples = 100;

// TODO
// Check for errors throughout, probably strictly 500 or at least anything but 200
// Exclude samples that err
// Filter out the "slowest"
// Order results by operation per second
// Pluck ops per second instead of name

//
// Shared App Data
//
var

  // Admin uname/pass dev/dev
  // Push Notifications, etc.
  admin = config.adminUsername + ":" + config.adminPass
  , deviceSerialNum = config.deviceSerialNum
  , username = deviceSerialNum
  , password = deviceSerialNum


  // The API endpoint to be concatted to specific requests
  , API = 'http://' + username + ":" + password + "@perftest.m3idash.com/Service/api/"
  , ADMIN_API = 'http://' + admin + "@perftest.m3idash.com/Service/api/"

  // Maximum Number of Responses
  , limit = 10

  , exportQueryName = "Authentication%20Report"

  // Use as "since" property in queries
  , timestamp = new Date('January 5 1995').toISOString()

  , token = {
    id: "89B34043A92678EC3C3ED56594C52A27A697BC9CE3AF447B088AA2569D2AF8BE"
  }

  , profile = {
    id: "1"
  }

  , datapoint = {
    id: "ExampleDomain",
    name: "ExampleName",
    value: "ExampleValue"
  }
  , datapoints = [
    {
      Name: 2,
      Value: "Washington"
    },
    {
      Name: 5,
      Value: "Lincoln"
    }
  ]
  , choice = datapoint.value

  // Filename to reflect
  , filename = "MyNewFile"

  // Extension to reflect
  , format = ".txt"

  // Content to reflect
  , content = "Arbitrary random ish text that will be used to generate a file"

  // True/False if content is Base64 encoded,
  , isBase64 = false

  , tagFilters = [
    {
      key: "Role", // A tag name to filter on
      value: "Developer" // A tag value to filter by
    },
    {
      key: "Company",
      value: "M3 Health"
    }
  ]

  , errors = {}
;

function *verifyResponse(err, res, body, name, request){
  if(!res) throw new Error('No Response for "'+request.method +' '+name+' ' + request.url + '. Error Processing Request "'+name+'"', err);

  // Only 200-range is valid.
  if(res.statusCode.indexOf(2) > -1 || err){
    throw new Error('Invalid Response Status for "'+request.method +' '+name+' ' + request.url + '" ' + res.statusCode + '. Error Processing Request "'+name+'"', err);
  } else {
    yield 200;
  }
};

//
//
//
var tests = {};
tests.admin = [
  /*
  {
    // GET api/Push/Alert/?message=<Message to display>
    name: 'Send pop-up alert to all registered devices',
    url: 'Push/Alert/?message=Test%20Message'
  },
  {
    // GET api/Push/Update
    name: 'Force "invisible" content update on all registered devices',
    url: 'Push/Update'
  },
  {
    // GET api/Push/Update/?id=<iDash Profile ID>
    name: 'Force "invisible" content update on user\'s device',
    url: 'Push/Update/?id=' + profile.id
  },
  {
    // GET api/Push/Delete
    name: 'Force "invisible" content deletion, reload on all registered devices',
    url: 'Push/Delete'
  },
  {
    // GET api/Push/Delete/?id=<iDash Profile ID>
    name: 'Force "invisible" content deletion, reload on user\'s device',
    url: 'Push/Delete/?id=' + profile.id
  },
  */
  {
    // GET api/Roster
    name: 'Export Current Roster as CSV',
    url: 'Roster/'
  }
];

tests.user = [
  {
    // GET api/Content/
    name: "List all User-Downloadable Content",
    url: "Content/"
  },
  {
    // GET api/Content/?newerThan=YYYY-MM-DDThh:mm:ss.sTZD
    name: 'List all newly-created User-Downloadable Content',
    url: 'Content/?newerThan=' + timestamp
  },
  {
    // POST api/Content/?forceS3=false
    // Requires an actual piece of content from s3
    //&filename=Test_Master/index.html',
    name: 'Retrieve pointer to a single piece of User-Downloadable Content',
    url: 'Content/?forceS3=false',
    headers: {
      'Content-Type': 'application/x-www-form-urlencoded',
      'Cache-Control': 'no-cache'
    },
    body: '=Test_Master%2Fcss%2Fmain.css',
    method: 'POST'
  },
  {
    // GET api/Datapoint/?id=<Datapoint Domain>&name=<Datapoint Code>&value=<Value to Store>
    name: 'Store Datapoint Value for User',
    url: 'Datapoint/?id='+datapoint.id+'&name='+datapoint.name+'&value='+datapoint.value
  },
  {
    // GET api/Datapoint/?id=<Datapoint Domain>&name=<Datapoint Code>
    name: 'Retrieve last Datapoint Value stored for User',
    url: 'Datapoint/?id=' + datapoint.id + "&name=" + datapoint.name
  },
  {
    // POST Datapoint/?id=<Datapoint Domain>&name=<Datapoint Code>
    // Body: datapoints%5B0%5D.Name%3D2%26datapoints%5B0%5D.Value%3D%22Washington%22%26datapoints%5B1%5D.Name%3D5%26datapoints%5B1%5D.Value%3D%22Lincoln%22
    name: 'Store Multiple Datapoint Values for User',
    url: 'Datapoint/?id='+datapoint.id,
    method: 'POST',
    body: encodeURI('datapoints[0].Name=2&datapoints[0].Value="Washington"&datapoints[1].Name=5&datapoints[1].Value="Lincoln"')
  },
  {
    // POST api/Download/?filename=<Filename to reflect>&format=<Extension to reflect>&content=<Content to reflect>&isbase64=<True/False if content is Base64 encoded>
    name: 'Service to allow Javascript to Reflect POSTed content as files to Browsers',
    url: 'Download/?filename='+filename+'&format='+format+'&content='+content+'&isbase64='+isBase64,
    method: 'POST'
  },
  {
    // GET api/Field/?name=<Profile Field Name>
    name: 'Get Extended Field Value for Current User Profile',
    url: 'Field/?name='+datapoint.name
  },
  {
    // GET api/Profile
    name: 'Get Extended Field Value for Current User Profile',
    url: 'Profile/'
  },
  /*
  nothing to point to
  {
    // GET api/Proxy
    // NOTE: Set the header "M3-Forward-To" to the target URL
    name: 'Provides a CORS-compliant proxy for HTTP/HTTPS requests (GET)',
    url: 'Proxy/',
    headers: {
        'M3-Forward-To': 'http://www.google.com'
    }
  },
  {
    // POST api/Proxy
    // NOTE: Set the header "M3-Forward-To" to the target URL
    name: 'Provides a CORS-compliant proxy for HTTP/HTTPS requests (POST)',
    url: 'Proxy/',
    headers: {
        'M3-Forward-To': 'http://www.google.com'
    },
    method: 'POST',
    body: JSON.stringify("nonsense")
  },
  */
  /*
   * needs to have an actual IPA
  {
    // GET api/PushToken/?id=<push token>
    name: 'Set Current User Push Token',
    url: 'PushToken/?id='+token.id
  },
  */
  {
    // GET api/PushToken
    name: 'Get Current User Push Token',
    url: 'PushToken/'
  },
  {
    // GET api/Report/Segments/?id=<Datapoint Domain>&field=<Profile Field Name>&name=<Datapoint Code>&choice=<Choice to filter on>
    name: 'Retrieve a list of respondent profile field values',
    url: 'Report/Segments/?id='+datapoint.id+'&field=id&name='+datapoint.name+'&choice='+choice
  },
  {
    // GET api/Report/Totals/?id=<Datapoint Domain>&name=<Datapoint Code>&tagFilters[0].Key=<A tag name to filter on>&tagFilters[0].Value=<A tag value to filter by>&tagFilters[1].Key=<A tag name to filter on>&tagFilters[1].Value=<A tag value to filter by>
    name: 'Retrieve a breakdown of (current) aggregated user responses',
    url: '/Report/Totals/?id='+datapoint.domain+'&name='+datapoint.name+'&tagFilters[0].Key='+tagFilters[0].key+'&tagFilters[0].Value='+tagFilters[0].value+'&tagFilters[1].Key='+tagFilters[1].key+'&tagFilters[1].Value='+tagFilters[1].value
  },
  {
    // GET api/Report/Count/?id=<Datapoint Domain>&name=<Datapoint Code>&tagFilters[0].Key=<A tag name to filter on>&tagFilters[0].Value=<A tag value to filter by>&tagFilters[1].Key=<A tag name to filter on>&tagFilters[1].Value=<A tag value to filter by>
    name: 'Retrieve a count of all users who responded to a datapoint',
    url: '/Report/Count/?id='+datapoint.domain+'&name='+datapoint.name+'&tagFilters[0].Key='+tagFilters[0].key+'&tagFilters[0].Value='+tagFilters[0].value+'&tagFilters[1].Key='+tagFilters[1].key+'&tagFilters[1].Value='+tagFilters[1].value
  },
  {
    // GET api/Report/Count/?id=<Datapoint Domain>&tagFilters[0].Key=<A tag name to filter on>&tagFilters[0].Value=<A tag value to filter by>&tagFilters[1].Key=<A tag name to filter on>&tagFilters[1].Value=<A tag value to filter by>
    name: 'Retrieve a count of all users who responded to the datapoints in a domain',
    url: '/Report/Count/?id='+datapoint.domain+'&tagFilters[0].Key='+tagFilters[0].key+'&tagFilters[0].Value='+tagFilters[0].value+'&tagFilters[1].Key='+tagFilters[1].key+'&tagFilters[1].Value='+tagFilters[1].value
  },
  {
    // GET api/Report/Sum/?id=<Datapoint Domain>&name=<Datapoint Code>&tagFilters[0].Key=<A tag name to filter on>&tagFilters[0].Value=<A tag value to filter by>&tagFilters[1].Key=<A tag name to filter on>&tagFilters[1].Value=<A tag value to filter by>
    name: 'Retrieve the sum total of all integer responses to a datapoint',
    url: '/Report/Sum/?id='+datapoint.domain+'&name='+datapoint.name+'&tagFilters[0].Key='+tagFilters[0].key+'&tagFilters[0].Value='+tagFilters[0].value+'&tagFilters[1].Key='+tagFilters[1].key+'&tagFilters[1].Value='+tagFilters[1].value
  },
  {
    // GET api/Report/Sum/?id=<Datapoint Domain>&tagFilters[0].Key=<A tag name to filter on>&tagFilters[0].Value=<A tag value to filter by>&tagFilters[1].Key=<A tag name to filter on>&tagFilters[1].Value=<A tag value to filter by>
    name: 'Retrieve the sum total of all integer responses to a datapoint excluding "name"',
    url: '/Report/Sum/?id='+datapoint.domain+'&tagFilters[0].Key='+tagFilters[0].key+'&tagFilters[0].Value='+tagFilters[0].value+'&tagFilters[1].Key='+tagFilters[1].key+'&tagFilters[1].Value='+tagFilters[1].value
  },
  {
    // GET api/Report/Sent/?id=<Datapoint Domain>
    name: 'Retrieve a count of responses submitted by the current user to the datapoints in a domain',
    url: 'Report/Sent/?id='+datapoint.id
  },
  {
    // GET api/Report/Recent/?id=<Datapoint Domain>&name=<Datapoint Code>&choice=<Choice to filter on>&limit=<Maximum Number of Responses>
    name: 'Retrieve a list of the newest response from each user matching the given criteria',
    url: 'Report/Recent/?id='+datapoint.id+'&name='+datapoint.name+'&choice='+choice+'&limit='+limit
  },
  {
    // GET api/Report/Comment/?id=<Datapoint Domain>&topic=<Datapoint Code>&since=YYYY-MM-DDThh:mm:ss.sTZD&limit=<Maximum Number of Responses>
    name: 'Retrieve a list of the latest responses matching the given criteria with identity information',
    url: 'Report/CommentFeed/?id='+datapoint.id+'&topic='+datapoint.name+'&since='+timestamp+'&limit='+limit
  },
  {
    // GET api/Report/Anonymous/?id=<Datapoint Domain>&topic=<Datapoint Code>&since=YYYY-MM-DDThh:mm:ss.sTZD&limit=<Maximum Number of Responses>
    name: 'Retrieve a list of the latest responses matching the given criteria without identity information',
    url: 'Report/AnonymousFeed/?id='+datapoint.id+'&topic='+datapoint.name+'&since='+timestamp+'&limit='+limit
  },
  {
    // GET api/Report/Export/?exportQueryName=<Predefined Query Name>
    name: 'Retrieve an XLSX document containing the results of a canned query',
    url: 'Report/Export/?exportQueryName='+exportQueryName
  },
  {
    // GET api/Settings
    name: 'List Settings',
    url: 'Settings/'
  }
];

/*
suite.add('Upload New Roster as CSV', {
  defer: true,
  fn: function(deferred){
    // POST api/Roster
    fs
    .createReadStream('CombinedRoster-Janssen-140725-1415EST.csv')
    .pipe(req({
      url: API + 'Roster/',
      method: 'POST'
    }, function(error, response, body){
      verifyResponse(error, response, body, 'Upload New Roster as CSV', deferred.resolve);
    }));
  }
})
*/

function testCb(apiUrl, item, index){
  // keep apiUrl in closure for configuration.
  return function(deferred){
    var request = {};

    request.url = apiUrl + item.url;
    request.method = item.method || 'GET';
    request.sendImmediately = false;

    if(item.body) request.body = item.body;
    if(item.headers) request.headers = item.headers;

    req(request, function(error, response, body){
      try {
        verifyResponse(error, response, body, item.name, request).next();
      } catch(err){
        errors[item.name.replace(' ', '')] = err;
      }

      deferred.resolve();
    });
  }
}

tests
.admin
.forEach(function(item, index){
  var cb = testCb(ADMIN_API, item, index);

  suite.add(item.name, {
    defer: true,
    fn: cb
  })
});

tests
.user
.forEach(function(item, index){
  var cb = testCb(API, item, index);

  suite.add(item.name, {
    defer: true,
    fn: cb
  })
});

suite
.on('cycle', function(event) {
  var stats = event.target.stats;

  console.log(String(event.target))
  console.log("---------STATS-----------");
  console.log("hz: " + event.target.hz);
  console.log("margin of error: " + stats.moe);
  console.log("relative margin of error: " + stats.rme);
  console.log("standard error of the mean: " + stats.sem);
  console.log("sample standard deviation: " + stats.deviation);
  console.log("mean: " + stats.mean);
  console.log("variance: " + stats.variance);
  console.log("-------------------------");
})
.on('complete', function() {
  console.log('Fastest is ' + this.filter('fastest').pluck('name'))

  var keys = Object.keys(errors);

  if(keys.length){
    console.log("\n\nThe following requests produced the following errors:")

    keys.forEach(function(item, index){
      console.log(index + ": " + errors[item]);
    });
  }
})
.run({
  async: config.async || false
});
